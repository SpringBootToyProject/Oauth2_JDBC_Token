package com.springtoyproject.oaut;

import org.springframework.http.MediaType;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.token.grant.password.ResourceOwnerPasswordResourceDetails;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class OauthGetTokenTest {
   /* @Ignore
    @Test
    public void getAccessTokenViaSpringSecurityOAuthClient() {
        try{

            ClientCredentialsResourceDetails resourceDetails = new ClientCredentialsResourceDetails();
            resourceDetails.setClientSecret("secret");
            resourceDetails.setClientId("web");
            resourceDetails.setAccessTokenUri("http://localhost:8181/oauth/token");
            resourceDetails.setScope(new ArrayList<String>(){{add("read");}});
            resourceDetails.setGrantType("password");



            OAuth2RestTemplate oAuthRestTemplate = new OAuth2RestTemplate(resourceDetails);

            org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
            headers.setContentType( MediaType.APPLICATION_JSON );

            OAuth2AccessToken token = oAuthRestTemplate.getAccessToken();
            System.out.println(oAuthRestTemplate.getResource());
            System.out.println(oAuthRestTemplate.getOAuth2ClientContext());
            System.out.println(token);

            assertTrue(token != null);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    //@Test
    //@Ignore
    public void clientAccess() {
        ResourceOwnerPasswordResourceDetails resourceDetails = new ResourceOwnerPasswordResourceDetails();
        resourceDetails.setUsername("username");
        resourceDetails.setPassword("password");
        resourceDetails.setAccessTokenUri("http://localhost:8181/oauth/token");
        resourceDetails.setClientId("web");
        resourceDetails.setClientSecret("secret");
        resourceDetails.setGrantType("password");
        resourceDetails.setScope(Arrays.asList("read"));

        DefaultOAuth2ClientContext clientContext = new DefaultOAuth2ClientContext();

        OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(resourceDetails, clientContext);
        org.springframework.http.HttpHeaders headers = new org.springframework.http.HttpHeaders();
        headers.setContentType( MediaType.APPLICATION_JSON );
        OAuth2AccessToken accessToken = restTemplate.getAccessToken();
        System.out.println("access token is: " + accessToken);
        /*restTemplate.setMessageConverters(asList(new MappingJackson2HttpMessageConverter()));

        final Greeting greeting = restTemplate.getForObject(format("http://localhost:%d/greeting", port), Greeting.class);

        System.out.println(greeting);*/
    }
  /*  @Test
    @Ignore
    public void testConnectDirectlyToResourceServer() throws Exception {

        Map<String, String[]> params = new HashMap<>();
        params.put("grant_type", new String[]{"password"});
        params.put("client_id", new String[]{"web"});
        params.put("username", new String[]{"username"});
        params.put("password", new String[]{"password"});
        ClientCredentialsResourceDetails resource = new ClientCredentialsResourceDetails();

        resource.setAccessTokenUri("http://localhost:8181/oauth/token");
        resource.setClientId("web");
       // resource.setId("sparklr");
        resource.setClientSecret("secret");
        resource.setScope(Arrays.asList("read"));

        ClientCredentialsAccessTokenProvider provider = new ClientCredentialsAccessTokenProvider();
        OAuth2AccessToken accessToken = provider.obtainAccessToken(resource, new DefaultAccessTokenRequest(params));
        System.out.println("token is: "+ accessToken);

        OAuth2RestTemplate template = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext(accessToken));
      //  String result = template.getForObject(serverRunning.getUrl("/sparklr2/photos/trusted/message"), String.class);
       // assertEquals("Hello, Trusted Client", result);

    }*/

}
