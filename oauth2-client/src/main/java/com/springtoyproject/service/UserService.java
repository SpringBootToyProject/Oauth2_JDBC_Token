package com.springtoyproject.service;

import com.springtoyproject.common.data.*;
import com.springtoyproject.common.exceptions.ErrorCodes;
import com.springtoyproject.common.exceptions.ErrorUtils;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.security.Role;
import com.springtoyproject.domain.security.User;
import com.springtoyproject.dto.AddUserDto;
import com.springtoyproject.dto.CreateUserDto;
import com.springtoyproject.dto.ShowGridUserDTO;
import com.springtoyproject.mapper.UserMapper;
import com.springtoyproject.repository.security.RoleRepository;
import com.springtoyproject.repository.security.SearchableSpecification;
import com.springtoyproject.repository.security.UserRepository;
import com.springtoyproject.service.clients.OauthProxyClient;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

@Service
public class UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository rolesRepository;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private OauthProxyClient oauthProxy;


    private Searchable[] searchables = new Searchable[]{
            new Searchable("userName", "userName", SearchableOperation.LIKE, SearchableType.STRING),
            new Searchable("dateFrom", "creationDate", SearchableOperation.BIGGER, SearchableType.DATE),
            new Searchable("dateTo", "creationDate", SearchableOperation.LESSER, SearchableType.DATE)

    };

    @Transactional
    public CreateUserDto create(CreateUserDto createUserDTO) throws ServiceException {

        if (StringUtils.isBlank(createUserDTO.getUserName())) {
            logger.error("Username cannot left blank in user");
            throw new ServiceException("Username cannot left blank in user", ErrorCodes.MISSING_PARAM.getCode());

        }
        if (userRepository.countByUserName(createUserDTO.getUserName()) > 0) {
            logger.error("The UserName:" + createUserDTO.getUserName() + " already exists in database ");
            throw new ServiceException("The UserName already exists in database", ErrorCodes.DUPLICATE_ENTRY.getCode());
        }

        try {
            oauthProxy.signUp(new UserDto().setUserName(createUserDTO.getUserName())
                    .setPassword(createUserDTO.getPassword()));
        } catch (Throwable t) {
                if (new ErrorUtils().isSameErrorCode(ErrorCodes.DUPLICATE_ENTRY, t))
                    throw new ServiceException("The UserName already exists in database. ", ErrorCodes.DUPLICATE_ENTRY.getCode());
                else
                    throw new ServiceException("user can not be created ", ErrorCodes.INTERNAL_ERROR.getCode());
            }

        User user = userMapper.createUserDTOToUser(createUserDTO);

        List<Role> roleList= new ArrayList<>();
        Role role_user = rolesRepository.findByName("ROLE_USER");
        if(role_user != null)
            roleList.add(role_user.setUsers(Collections.singletonList(user)));

        else roleList.add(new Role().setUsers(Collections.singletonList(user)).setName("ROLE_USER"));

        user.setRoles(roleList);

        User result = userRepository.save(user);

        logger.info("create", createUserDTO.getClass().getName(), createUserDTO, result);

        return new CreateUserDto().setUserName(result.getUserName());


    }

    @Transactional
    public AddUserDto add(AddUserDto userDto) throws ServiceException {

        String userName = userDto.getUserName();
        if (StringUtils.isBlank(userName)) {
            throw new ServiceException("Username cannot left blank in user", ErrorCodes.MISSING_PARAM.getCode());

        }
        if (userRepository.countByUserName(userName) > 0) {
            throw new ServiceException("The UserName already exists in database", ErrorCodes.DUPLICATE_ENTRY.getCode());
        }

        try {
            oauthProxy.add(new SimpleRequestDto().setKey(userName));

        } catch (Throwable t) {
            if (!new ErrorUtils().isSameErrorCode(ErrorCodes.DUPLICATE_ENTRY, t)) {
                logger.error( "create", SimpleRequestDto.class.getName(), userDto,

                        "The UserName:" + userName + " can not be created");
                throw new ServiceException("user can not be created", ErrorCodes.UNKNOWN.getCode());
            }
        }

        User user = userMapper.addUserDTOToUser(userDto);

        List<Role> roleList= new ArrayList<>();
        for (RoleDto roledto : userDto.getRoles()) {
            Role role = rolesRepository.findOne(roledto.getId());
            if (role != null) {
                roleList.add(role);
            } else {
                logger.error("role not found with id :" + roledto.getId());
            }
        }

        user.setRoles(roleList);
        User result = userRepository.save(user);

        return userMapper.userToAddUserDTO(result);
    }

    @Transactional(readOnly = true)
    public Page<ShowGridUserDTO> findAll(int page, int total, String order, String direction, Map<String, String> filter) throws ServiceException {
        Sort.Direction dir = getDirection(direction);
        PageRequest pageRequest = new PageRequest(page, total, dir, order);

        List<Searchable> criteria = SearchUtils.createSearchableFrom(filter, searchables);
        try {
            Page<User> users = userRepository.findAll(new SearchableSpecification<User>(criteria), pageRequest);
            List<ShowGridUserDTO> userDTOs = new ArrayList<>();
            users.forEach(user -> userDTOs.add(userMapper.userToShowGridUserDTO(user)));

            return new ResultPageDto<>(users, userDTOs);

        } catch (PropertyReferenceException exp) {
            logger.error( "An error occurred while querying the database");
            exp.printStackTrace();
            throw new ServiceException("An error occurred while querying the database", ErrorCodes.PROPERTY_REFERENCE_ERROR.getCode());

        } catch (Exception exp) {
            logger.error( "An error occurred while querying the database");
            exp.printStackTrace();
            throw new ServiceException("An error occurred while querying the database", ErrorCodes.INTERNAL_ERROR.getCode());
        }
    }

    private Sort.Direction getDirection(String direction) throws ServiceException {
        Sort.Direction dir;
        try {
            dir = Sort.Direction.fromString(direction);

        } catch (Exception exp) {
            exp.printStackTrace();
            throw new ServiceException("Please make sure direction is valid ", ErrorCodes.INTERNAL_ERROR.getCode());
        }
        return dir;
    }
}
