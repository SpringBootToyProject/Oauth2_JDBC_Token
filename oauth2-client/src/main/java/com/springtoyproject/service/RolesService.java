package com.springtoyproject.service;

import com.springtoyproject.common.data.*;
import com.springtoyproject.common.exceptions.ErrorCodes;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.security.Role;
import com.springtoyproject.domain.security.RolePermission;
import com.springtoyproject.domain.security.User;
import com.springtoyproject.dto.ShowDetailRoleDto;
import com.springtoyproject.mapper.RoleMapper;
import com.springtoyproject.repository.security.RoleRepository;
import com.springtoyproject.repository.security.SearchableSpecification;
import com.springtoyproject.repository.security.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class RolesService {
    private final org.slf4j.Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private RoleMapper roleMapper;

    @Autowired
    private UserRepository userRepository;


    private Searchable[] searchables = new Searchable[]{
            new Searchable("name", "name", SearchableOperation.LIKE, SearchableType.STRING),
            new Searchable("dateFrom", "creationDate", SearchableOperation.BIGGER, SearchableType.DATE),
            new Searchable("dateTo", "creationDate", SearchableOperation.LESSER, SearchableType.DATE)
    };

    @Transactional
    public CreateRoleDto create(CreateRoleDto roleDto) throws ServiceException {

        if (roleRepository.findOne(roleDto.getId()) != null) {
            logger.error( "create", roleDto.getClass().getName(), roleDto, "The role already exists ");
            throw new ServiceException("The role already exists ", ErrorCodes.DUPLICATE_ENTRY.getCode());
        }

        if (roleRepository.findByName(roleDto.getName()) != null) {
            logger.error( "create", roleDto.getClass().getName(), roleDto, "The role Name already exists ");
            throw new ServiceException("The role Name already exists ", ErrorCodes.DUPLICATE_ENTRY.getCode());
        }

        Role role = roleMapper.createRoleToRole(roleDto);

        if (roleDto.getPermissions() != null)
            for (String permission : roleDto.getPermissions())
                role.getPermissions().add(new RolePermission().setPermission(permission).setRole(role));

        return roleMapper.roleToCreateRoleDto(roleRepository.save(role));
    }

    @Transactional
    public CreateRoleDto update(CreateRoleDto roleDto) throws ServiceException {


        if (roleRepository.findOne(roleDto.getId()) == null) {
            logger.error( "update", roleDto.getClass().getName(), roleDto, "The role does not exists");
            throw new ServiceException("The role does not exists", ErrorCodes.NO_ENTITY.getCode());
        }

        if (roleRepository.countByNameAndIdNotIn(roleDto.getName(), roleDto.getId()) > 0) {
            logger.error( "create", roleDto.getClass().getName(), roleDto, "The role Name already exists ");
            throw new ServiceException("The role Name already exists ", ErrorCodes.DUPLICATE_ENTRY.getCode());
        }

        Role role = roleMapper.createRoleToRole(roleDto);
        if (roleDto.getPermissions() != null) {
            role.getPermissions().clear();
            for (String permission : roleDto.getPermissions())
                role.getPermissions().add(new RolePermission().setPermission(permission).setRole(role));
        }
        return roleMapper.roleToCreateRoleDto(roleRepository.save(role));
    }

    @Transactional
    public void delete(long roleId) throws ServiceException {

        if (roleRepository.findOne(roleId) == null) {
           logger.error( "delete", "long roleId", roleId, "The role does not exists");
            throw new ServiceException("The role do not exists", ErrorCodes.NO_ENTITY.getCode());
        }
        try {
            roleRepository.delete(roleId);
            logger.info("role with roleId : " + roleId + "deleted successfully. ");
        } catch (Exception exp) {
            exp.printStackTrace();
           logger.error( "delete", "long roleId", roleId, "Cannot delete the entity, most like is used by another entity.");
            throw new ServiceException("Cannot delete the entity, most like is used by another entity.", ErrorCodes.INTERNAL_ERROR.getCode());
        }
    }

    @Transactional(readOnly = true)
    public Page<RoleDto> findAll(int page, int total, String order, String direction, Map<String, String> filter) throws ServiceException {
        Sort.Direction dir = getDirection(direction);

        PageRequest pageRequest = new PageRequest(page, total, dir, order);

        List<Searchable> criteria = SearchUtils.createSearchableFrom(filter, searchables);

        try {
            Page<Role> roles = roleRepository.findAll(new SearchableSpecification<Role>(criteria), pageRequest);
            List<RoleDto> roleDtos = new ArrayList<>();
            roles.map(role -> roleDtos.add(new RoleDto().setDescription(role.getDescription()).setId(role.getId()).setName(role.getName())));

            return new ResultPageDto<>(roles, roleDtos);
        } catch (PropertyReferenceException exp) {
           logger.error( "findAll", null, null, "An error occurred while querying the database");
            exp.printStackTrace();
            throw new ServiceException("An error occurred while querying the database", ErrorCodes.PROPERTY_REFERENCE_ERROR.getCode());

        } catch (Exception exp) {
           logger.error( "findAll", null, null, "An error occurred while querying the database");
            exp.printStackTrace();
            throw new ServiceException("An error occurred while querying the database", ErrorCodes.INTERNAL_ERROR.getCode());
        }
    }

    @Transactional(readOnly = true)
    public ShowDetailRoleDto find(String name) {
        Role role = roleRepository.findByName(name);
        return roleMapper.RoleToShowDetailRoleDto(role);

    }

    @Transactional(readOnly = true)
    public ShowDetailRoleDto find(long id) {
        Role role = roleRepository.findOne(id);
        return roleMapper.RoleToShowDetailRoleDto(role);
    }

    @Transactional(readOnly = true)
    public List<ShowDetailRoleDto> userRoles(String userName) throws ServiceException {

        Optional<User> user = userRepository.findByUserName(userName);
        if (!user.isPresent()) {
           logger.error( "userRoles", "String userName", userName,
                    "User cannot be found. userName:" + userName);
            throw new ServiceException("User cannot be found", ErrorCodes.NO_ENTITY.getCode());
        }

        return roleMapper.RolesToShowDetailRoleDtos(user.get().getRoles());
    }

    @Transactional
    public List<ShowDetailRoleDto> updateUserRoles(String userName, List<RoleDto> roles) throws ServiceException {

        Optional<User> optUser = userRepository.findByUserName(userName);
        if (!optUser.isPresent()) {
           logger.error( "find", userName.getClass().getName(), userName,
                    "User cannot be found. userName:" + userName);
            throw new ServiceException("User cannot be found", ErrorCodes.NO_ENTITY.getCode());
        }
        User user = optUser.get();
        List<Role> userRoles = new ArrayList<>();
        for (RoleDto dto : roles) {
            Role role = roleRepository.findOne(dto.getId());
            if (role != null) {
                userRoles.add(role);
            }
        }
        user.getRoles().clear();
        user.setRoles(userRoles);
        User result = userRepository.save(user);

        return roleMapper.RolesToShowDetailRoleDtos(result.getRoles());
    }

    private Sort.Direction getDirection(String direction) throws ServiceException {
        Sort.Direction dir;
        try {
            dir = Sort.Direction.fromString(direction);

        } catch (Exception exp) {
            exp.printStackTrace();
           logger.error( "getDirection", "String direction", direction,
                    "Please make sure direction is valid ");
            throw new ServiceException("Please make sure direction is valid ", ErrorCodes.INTERNAL_ERROR.getCode());
        }
        return dir;
    }

}
