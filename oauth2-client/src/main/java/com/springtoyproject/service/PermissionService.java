package com.springtoyproject.service;

import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.enums.Oauth2ClientPermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


@Service
@Transactional
public class PermissionService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());


    public List<Oauth2ClientPermission> findAll() throws ServiceException {
        logger.info("Request to findAll permission. ");

        List<Oauth2ClientPermission> permissions = new ArrayList<>();

        permissions.addAll(Arrays.asList(Oauth2ClientPermission.values()));

        return permissions;
    }
}
