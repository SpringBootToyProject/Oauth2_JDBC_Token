package com.springtoyproject.service.clients;

import com.springtoyproject.common.data.SimpleRequestDto;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.common.data.UserDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

//@FeignClient(value = "oauth",url = "http://localhost:8181")
@FeignClient(value = "http://oauth")
public interface OauthProxyClient {

    @RequestMapping(method = RequestMethod.POST,value = "/api/v1/users/signUp",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity signUp(@Valid @RequestBody UserDto userDto) throws ServiceException;


    @RequestMapping(method = RequestMethod.POST, value = "/api/v1/users/add",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    ResponseEntity add(@Valid @RequestBody SimpleRequestDto dto) throws ServiceException;


}
