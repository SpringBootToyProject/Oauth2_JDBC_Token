package com.springtoyproject.domain.enums;


public enum Oauth2ClientPermission {
    DEFAULT("", false),

    OAUTH2CLIENT_ROLE_CREATE("", false),
    OAUTH2CLIENT_ROLE_UPDATE("", false),
    OAUTH2CLIENT_ROLE_GET("", false),
    OAUTH2CLIENT_ROLE_DELETE("", false),
    OAUTH2CLIENT_ROLE_LIST("", false),

    OAUTH2CLIENT_USERROLE_GET("", false),
    OAUTH2CLIENT_USERROLE_SET("", false),

    OAUTH2CLIENT_PERMISSION_LIST("", false),

    OAUTH2CLIENT_USER_UPDATE("", false),
    OAUTH2CLIENT_USER_CREATE("", false),
    OAUTH2CLIENT_USER_LIST("", false),
    OAUTH2CLIENT_USER_DETAILS("", false),
    OAUTH2CLIENT_USER_DELETE("", false),


    OAUTH2CLIENT_HOTEL_CREATE("", false),
    OAUTH2CLIENT_HOTEL_UPDATE("", false),
    OAUTH2CLIENT_HOTEL_DELETE("", false),
    OAUTH2CLIENT_HOTEL_LIST("", false),
    OAUTH2CLIENT_HOTEL_GET("", false),


    OAUTH2CLIENT_HOTEL_PACKAGE_DELETE("", false),


    OAUTH2CLIENT_ROOM_TYPE_CREATE("", false),

    OAUTH2CLIENT_ROOM_TYPE_DELETE("", false),
    OAUTH2CLIENT_ROOM_TYPE_LIST("", false),
    OAUTH2CLIENT_ROOM_TYPE_GET("", false),
    OAUTH2CLIENT_ROOM_TYPE_UPDATE("",false ),



    OAUTH2CLIENT_TOUR_PACKAGE_CREATE("", false),
    OAUTH2CLIENT_TOUR_PACKAGE_DELETE("", false),
    OAUTH2CLIENT_TOUR_PACKAGE_ADMIN_LIST("", false),
    OAUTH2CLIENT_TOUR_PACKAGE_USER_LIST("", false),
    OAUTH2CLIENT_TOUR_PACKAGE_GET("", false),
    OAUTH2CLIENT_AGENCY_CREATE("", false),
    OAUTH2CLIENT_AGENCY_UPDATE("", false),
    OAUTH2CLIENT_AGENCY_DELETE("", false),
    OAUTH2CLIENT_AGENCY_LIST("", false),
    OAUTH2CLIENT_AGENCY_GET("", false),

    OAUTH2CLIENT_CHANGE_STATUS("", false),
    OAUTH2CLIENT_VOUCHER_DETAIL("", false),
    OAUTH2CLIENT_BOOK_LIST("", false),
    OAUTH2CLIENT_USER_BOOK_LIST("", false),


    OAUTH2CLIENT_LANGUAGE_CREATE("", false),
    OAUTH2CLIENT_LANGUAGE_UPDATE("", false),
    OAUTH2CLIENT_LANGUAGE_GET("", false),
    OAUTH2CLIENT_LANGUAGE_LIST("", false),
    OAUTH2CLIENT_LANGUAGE_DELETE("", false),
    OAUTH2CLIENT_LANGUAGE_CHANGEDEFAULT("", false),


    OAUTH2CLIENT_LICIENCETYPE_CREATE("",false ),
    OAUTH2CLIENT_LICIENCETYPE_UPDATE("", false),
    OAUTH2CLIENT_LICIENCETYPE_GET("", false),
    OAUTH2CLIENT_LICIENCETYPE_LIST("", false),
    OAUTH2CLIENT_LICIENCETYPE_DELETE("", false),
    OAUTH2CLIENT_CURRENCY_CREATE("",false );


    private final String description;
    private final boolean rowLevel;

    Oauth2ClientPermission(String description, boolean rowLevel) {
        this.description = description;
        this.rowLevel = rowLevel;
    }

    public String getDescription() {
        return description;
    }

    public boolean isRowLevel() {
        return rowLevel;
    }

    @Override
    public String toString() {
        return "Oauth2ClientPermission{" +
                "description='" + description + '\'' +
                ", rowLevel=" + rowLevel +
                '}';
    }


}




