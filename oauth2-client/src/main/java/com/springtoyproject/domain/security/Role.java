package com.springtoyproject.domain.security;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "smp_roles")
public class Role implements Serializable {

    @Id
    @SequenceGenerator(name = "smp_roles_id_seq", sequenceName = "smp_roles_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "smp_roles_id_seq")
    @Column
    private long id;

    @NotNull
    @Column(columnDefinition = "nvarchar2(255)")
    private String name;

    @Column(columnDefinition = "CLOB")
    private String description;

    @ManyToMany(mappedBy = "roles",cascade = CascadeType.ALL)
    @OrderBy("id")
    private List<User> users = new ArrayList<>();


    @OneToMany(cascade = CascadeType.ALL, mappedBy = "role", orphanRemoval = true)
    private List<RolePermission> permissions = new ArrayList<>();


    @CreatedDate
    @Column(name = "creation_date")
    @Temporal(TemporalType.DATE)
    private Date creationDate = new Date();

    public long getId() {
        return id;
    }

    public Role setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public Role setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Role setDescription(String description) {
        this.description = description;
        return this;
    }

    public List<RolePermission> getPermissions() {
        return permissions;
    }

    public Role setPermissions(List<RolePermission> permissions) {
        this.permissions = permissions;
        return this;
    }

    public List<User> getUsers() {
        return users;
    }

    public Role setUsers(List<User> users) {
        this.users = users;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        return !(name != null ? !name.equals(role.name) : role.name != null);

    }

    @Override
    public int hashCode() {
        return (int) (id ^ (id >>> 32));
    }
}
