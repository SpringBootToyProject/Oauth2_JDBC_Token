package com.springtoyproject.domain.security;


import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "smp_role_permissions")
public class RolePermission implements Serializable {

    @Id
    @SequenceGenerator(name = "smp_role_permissions_id_seq", sequenceName = "smp_role_permissions_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "smp_role_permissions_id_seq")
    @Column
    private long id;


    @ManyToOne
    @JoinColumn(columnDefinition = "role_fk", referencedColumnName = "id")
    private Role role;

    @Column
    private String permission;


    @CreatedDate
    @Column(name = "creation_date")
    @Temporal(TemporalType.DATE)
    private Date creationDate = new Date();

    public RolePermission() {
    }

    public long getId() {
        return id;
    }

    public RolePermission setId(long id) {
        this.id = id;
        return this;
    }

    public Role getRole() {
        return role;
    }

    public RolePermission setRole(Role role) {
        this.role = role;
        return this;
    }

    public String getPermission() {
        return permission;
    }

    public RolePermission setPermission(String permission) {
        this.permission = permission;
        return this;
    }


    @Override
    public String toString() {
        return "RolePermission{" +
                "id=" + id +
                ", role=" + role +
                ", permission='" + permission + '\'' +
                '}';
    }
}
