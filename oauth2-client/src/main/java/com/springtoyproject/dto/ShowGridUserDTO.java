package com.springtoyproject.dto;



import java.io.Serializable;

public class ShowGridUserDTO implements Serializable {
    private static final long serialVersionUID = 1L;

    private String userName;


    public ShowGridUserDTO() {
    }


    public static long getSerialVersionUID() {
        return serialVersionUID;
    }


    public String getUserName() {
        return userName;
    }

    public ShowGridUserDTO setUserName(String userName) {
        this.userName = userName;
        return this;
    }



    @Override
    public String toString() {
        return "ShowGridUserDTO{" +
                "userName='" + userName + '\'' +

                '}';
    }
}
