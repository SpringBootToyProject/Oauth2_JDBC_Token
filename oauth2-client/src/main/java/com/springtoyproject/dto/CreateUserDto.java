package com.springtoyproject.dto;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

public class CreateUserDto implements Serializable {

    //region properties

    @NotNull(message = "NotNull.CreateUserDto.userName")
    private String userName;

    @NotNull(message = "NotNull.CreateUserDto.password")
    private String password;

    private boolean enabled = true;

    //endregion

    //region constructor

    public CreateUserDto() {
    }
    //endregion

    //region getter and setter

    public String getUserName() {
        return userName;
    }

    public CreateUserDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public CreateUserDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public CreateUserDto setEnabled(boolean enabled) {
        this.enabled = enabled;
        return this;
    }



    //endregion


    @Override
    public String toString() {
        return "CreateUserDto{" +
                " userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", enabled=" + enabled +
                '}';
    }
}
