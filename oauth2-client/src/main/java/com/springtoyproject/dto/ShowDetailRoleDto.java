package com.springtoyproject.dto;


import java.util.HashSet;
import java.util.Set;

public class ShowDetailRoleDto {

    private String name;
    private String description;
    private Set<RolePermissionDto> permissions= new HashSet<RolePermissionDto>();

    public String getName() {
        return name;
    }

    public ShowDetailRoleDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ShowDetailRoleDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public Set<RolePermissionDto> getPermissions() {
        return permissions;
    }

    public ShowDetailRoleDto setPermissions(Set<RolePermissionDto> permissions) {
        this.permissions = permissions;
        return this;
    }
}
