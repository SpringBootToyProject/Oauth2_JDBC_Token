package com.springtoyproject.dto;


import com.springtoyproject.common.data.RoleDto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class AddUserDto implements Serializable {

    @NotNull
    @Size(min = 3)
    private String userName;

    @NotNull
    private List<RoleDto> roles;

    public String getUserName() {
        return userName;
    }

    public AddUserDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }



    public List<RoleDto> getRoles() {
        return roles;
    }

    public AddUserDto setRoles(List<RoleDto> roles) {
        this.roles = roles;
        return this;
    }

    @Override
    public String toString() {
        return "AddUserDTO{" +
                "userName='" + userName + '\'' +
                ", roles=" + roles +
                '}';
    }
}
