package com.springtoyproject.dto;


public class RolePermissionDto {

    private long id;
    private String permission;

    public long getId() {
        return id;
    }

    public RolePermissionDto setId(long id) {
        this.id = id;
        return this;
    }

    public String getPermission() {
        return permission;
    }

    public RolePermissionDto setPermission(String permission) {
        this.permission = permission;
        return this;
    }
}
