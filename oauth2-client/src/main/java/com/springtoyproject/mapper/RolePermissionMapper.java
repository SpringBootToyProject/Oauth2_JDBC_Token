package com.springtoyproject.mapper;


import com.springtoyproject.domain.security.RolePermission;
import com.springtoyproject.dto.RolePermissionDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring", uses = {}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RolePermissionMapper {

    RolePermissionDto RolePermissionDtoToRolePermission(RolePermission rolePermission);

    RolePermission RolePermissionDtoToRolePermission(RolePermissionDto rolePermissionDto);


}
