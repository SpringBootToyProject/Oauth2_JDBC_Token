package com.springtoyproject.mapper;

import com.springtoyproject.domain.security.User;
import com.springtoyproject.dto.AddUserDto;
import com.springtoyproject.dto.CreateUserDto;
import com.springtoyproject.dto.ShowGridUserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;


@Mapper(componentModel = "spring", uses = {RoleMapper.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserMapper {
    User createUserDTOToUser(CreateUserDto createUserDTO);

    User addUserDTOToUser(AddUserDto userDto);

    AddUserDto userToAddUserDTO(User result);

    ShowGridUserDTO userToShowGridUserDTO(User User);
}
