package com.springtoyproject.mapper;

import com.springtoyproject.common.data.CreateRoleDto;
import com.springtoyproject.domain.security.Role;
import com.springtoyproject.common.data.RoleDto;
import com.springtoyproject.dto.ShowDetailRoleDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

import java.util.List;


@Mapper(componentModel = "spring", uses = {RolePermissionMapper.class}, unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface RoleMapper {

    RoleDto roleToRoleDTO(Role Role);

    Role roleToRoleDTO(RoleDto Role);

    List<RoleDto> rolesToRoleDTOs(List<Role> Role);

    @Mappings({
            @Mapping(target = "permissions", ignore = true)
    })
    Role createRoleToRole(CreateRoleDto roleDto);

    @Mappings({
            @Mapping(target = "permissions", ignore = true)
    })
    CreateRoleDto roleToCreateRoleDto(Role role);


    ShowDetailRoleDto RoleToShowDetailRoleDto(Role role);

    List<ShowDetailRoleDto> RolesToShowDetailRoleDtos(List<Role> roles);


}

