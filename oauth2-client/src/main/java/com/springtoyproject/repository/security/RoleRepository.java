package com.springtoyproject.repository.security;

import com.springtoyproject.domain.security.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long>, JpaSpecificationExecutor {

    Role findByName(String name);

    long countByName(String name);

    long countByNameAndIdNotIn(String name, Long id);

   /* @Query("select r.name from Role r  inner join r.users  u where u.userName=:userName")
    List<Role> findByUserName(@Param("userName") String userName);*/


}