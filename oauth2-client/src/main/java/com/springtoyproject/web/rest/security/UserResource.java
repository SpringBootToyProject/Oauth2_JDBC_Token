package com.springtoyproject.web.rest.security;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.springtoyproject.common.data.UserDto;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.enums.Oauth2ClientPermission;
import com.springtoyproject.dto.AddUserDto;
import com.springtoyproject.dto.CreateUserDto;
import com.springtoyproject.dto.ShowGridUserDTO;
import com.springtoyproject.security.CustomPermissionProvider;
import com.springtoyproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(value = "/api/v1/users")
public class UserResource {


    @Autowired
    private UserService userService;


    @Autowired
    private CustomPermissionProvider permissionProvider;

    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> register(@Valid @RequestBody CreateUserDto createUserDto) throws ServiceException, JsonProcessingException {

        return new ResponseEntity<>(userService.create(createUserDto), HttpStatus.OK);
    }


    //Admin creates a user and store it in the storage. Required role is PMS_USER_CREATE
    @RequestMapping(value = "/add", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> add(@RequestHeader String Authorization, @Valid @RequestBody AddUserDto user)
            throws ServiceException, JsonProcessingException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_USER_CREATE);
        return new ResponseEntity<>(userService.add(user), HttpStatus.OK);
    }


    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Page<ShowGridUserDTO>> list(@RequestHeader String Authorization,
                                                      @RequestParam int page,
                                                      @RequestParam int total,
                                                      @RequestParam String order,
                                                      @RequestParam String direction,
                                                      @RequestParam(required = false) String userName,
                                                      @RequestParam(required = false) String dateFrom,
                                                      @RequestParam(required = false) String dateTo
                                                      )
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_USER_LIST);

        Map<String, String> query = new HashMap<>();
        query.put("userName", userName);
        query.put("dateFrom", dateFrom);
        query.put("dateTo", dateTo);

        return new ResponseEntity<>(userService.findAll(page, total, order, direction, query), HttpStatus.OK);
    }

}
