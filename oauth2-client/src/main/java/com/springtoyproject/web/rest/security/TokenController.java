package com.springtoyproject.web.rest.security;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(path = "/api/v1")
public class TokenController {
    private static final Logger logger = LoggerFactory.getLogger(TokenController.class);

    @PostMapping("/getToken")
    public String obtainAccessToken(@RequestParam String userName,
                                    @RequestParam String password) {
        logger.info("Request to get token by userName: " + userName);
        Map<String, String> params = new HashMap<>();
        params.put("grant_type", "password");
        params.put("client_id", "web");
        params.put("username", userName);
        params.put("password", password);
        Response response = RestAssured.given().auth().preemptive()
                .basic("web", "secret").and().with().params(params)
                .when()
                .post("http://localhost:8181/oauth/token");
        return response.jsonPath().getString("access_token");
    }




}




