package com.springtoyproject.web.rest.security;

import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.enums.Oauth2ClientPermission;
import com.springtoyproject.common.data.CreateRoleDto;
import com.springtoyproject.common.data.RoleDto;
import com.springtoyproject.dto.ShowDetailRoleDto;
import com.springtoyproject.security.CustomPermissionProvider;
import com.springtoyproject.service.RolesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping(value = "/api/v1/roles")
public class RolesResource {

    @Autowired
    private RolesService rolesService;

    @Autowired
    private CustomPermissionProvider permissionProvider;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<CreateRoleDto> create(@RequestHeader String Authorization, @Valid @RequestBody CreateRoleDto role)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_ROLE_CREATE);

        return new ResponseEntity<>(rolesService.create(role), HttpStatus.OK);
    }

    @RequestMapping(value = "/update", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<CreateRoleDto> update(@RequestHeader String Authorization, @Valid @RequestBody CreateRoleDto role)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_ROLE_UPDATE);

        return new ResponseEntity<>(rolesService.update(role), HttpStatus.OK);
    }

    @RequestMapping(value = "/details/{role}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<ShowDetailRoleDto> getRole(@RequestHeader String Authorization, @PathVariable long role)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_ROLE_GET);

        return new ResponseEntity<>(rolesService.find(role), HttpStatus.OK);
    }

    @RequestMapping(value = "/delete/{role}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity delete(@RequestHeader String Authorization, @PathVariable long role)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_ROLE_DELETE);

        rolesService.delete(role);

        return new ResponseEntity(HttpStatus.OK);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<Page<RoleDto>> list(@RequestHeader String Authorization,
                                              @RequestParam int page,
                                              @RequestParam int total,
                                              @RequestParam String order,
                                              @RequestParam String direction,
                                              @RequestParam(required = false) String dateFrom,
                                              @RequestParam(required = false) String dateTo,
                                              @RequestParam(required = false) String name)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_ROLE_LIST);

        Map<String, String> query = new HashMap<>();
        query.put("dateFrom", dateFrom);
        query.put("dateTo", dateTo);
        query.put("name", name);

        return new ResponseEntity<>(rolesService.findAll(page, total, order, direction, query), HttpStatus.OK);
    }

    @RequestMapping(value = "/user/{user}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> userRoles(
            @RequestHeader String Authorization,
            @PathVariable String user)
            throws ServiceException {

        permissionProvider.isAllowed(Oauth2ClientPermission.OAUTH2CLIENT_USERROLE_GET);

        return new ResponseEntity<>(rolesService.userRoles(user), HttpStatus.OK);
    }
}

