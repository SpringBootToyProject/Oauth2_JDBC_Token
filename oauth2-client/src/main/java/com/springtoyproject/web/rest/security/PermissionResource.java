package com.springtoyproject.web.rest.security;

import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.enums.Oauth2ClientPermission;
import com.springtoyproject.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1")
public class PermissionResource {

    @Autowired
    private PermissionService permissionService;

    @RequestMapping(value = "/roles/permissions/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<List<Oauth2ClientPermission>> permissionList(
            @RequestHeader String Authorization
    ) throws ServiceException {

        // permissionProvider.isAllowed(PMSPermission.PMS_PERMISSION_LIST);

        return new ResponseEntity<>(permissionService.findAll(), HttpStatus.OK);
    }
}
