package com.springtoyproject.security;


import com.springtoyproject.common.exceptions.ErrorCodes;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.config.SecurityUtils;
import com.springtoyproject.domain.enums.Oauth2ClientPermission;
import com.springtoyproject.domain.security.Role;
import com.springtoyproject.domain.security.RolePermission;
import com.springtoyproject.domain.security.User;
import com.springtoyproject.repository.security.UserRepository;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Ehcache;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration;
import net.sf.ehcache.store.MemoryStoreEvictionPolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CustomPermissionProvider {

    private static Logger logger = Logger.getLogger(CustomPermissionProvider.class.getName());

    @Autowired
    private UserRepository userRepository;


    static synchronized Ehcache fetchCache() {
        String cacheName = "pmsPermissionCache";
        Ehcache ehcache;

        if (CacheManager.getInstance().cacheExists(cacheName)) {
            ehcache = CacheManager.getInstance().getCache(cacheName);

        } else {
            logger.info("Creating cache for caching permission list ");

            CacheConfiguration config = new CacheConfiguration(cacheName, 100000)
                    .memoryStoreEvictionPolicy(MemoryStoreEvictionPolicy.LFU)
                    .eternal(false)
                    .timeToLiveSeconds(60 * 1)
                    .timeToIdleSeconds(60 * 1)
                    .persistence(new PersistenceConfiguration().strategy(PersistenceConfiguration.Strategy.LOCALTEMPSWAP));

            Cache cache = new Cache(config);
            CacheManager.create().addCache(cache);
            ehcache = cache;
        }
        return ehcache;
    }
    @Transactional
    public boolean isAllowed(Oauth2ClientPermission pl) throws ServiceException {

        String permission = pl.name();

        String principle = SecurityUtils.getCurrentUserName();

        logger.info(principle + " is checking for permission of " + permission);

        if (StringUtils.isBlank(principle))
            throw new ServiceException("User is not know ", ErrorCodes.NO_USER.getCode());

        Element element = fetchCache().get(principle);
        if (element == null) {
            logger.info("Cannot find the permission for " + principle + " in cache, loading from database");

            Optional<User> optUser = userRepository.findByUserName(principle);
            if (!optUser.isPresent())
                throw new ServiceException("Principle is not known ", ErrorCodes.NO_USER.getCode());

            User user = optUser.get();

            element = new Element(principle, user.getRoles());
            fetchCache().put(element);
        }

        List<Role> roles = (List<Role>) element.getObjectValue();
        if (roles == null)
            throw new ServiceException("Role list is not know ", ErrorCodes.ACCESS_DENIED.getCode());

        for (Role role : roles)
            for (RolePermission p : role.getPermissions())
                if (StringUtils.equals(p.getPermission(), permission))
                    return true;

        throw new ServiceException("User is not allowed", ErrorCodes.ACCESS_DENIED.getCode());
    }

}
