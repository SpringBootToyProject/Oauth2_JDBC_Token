package com.springtoyproject.config;

import org.springframework.security.core.context.SecurityContextHolder;


public final class SecurityUtils {

    public static String getCurrentUserName() {
        try {
            return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();
        } catch (Exception e) {
            return null;
        }
    }
}

