package com.springtoyproject.service;

import com.springtoyproject.repository.UserRepository;
import com.springtoyproject.repository.UserRolesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service
public class EnhanceUserDetailsService implements UserDetailsService {

    private UserRepository userRepository;
    private final UserRolesRepository userRolesRepository;


    @Autowired
    public EnhanceUserDetailsService(UserRepository userRepository, UserRolesRepository userRolesRepository) {
        this.userRepository = userRepository;
        this.userRolesRepository = userRolesRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Set<GrantedAuthority> grantedAuthorities = (userRolesRepository.findRoleByUserName(username)).stream()
                .map(SimpleGrantedAuthority::new).collect(Collectors.toSet());
        return userRepository
                .findByUserName(username)
                .map(user -> new User(user.getUserName(), user.getPassword(), grantedAuthorities))
                .orElseThrow(() -> new UsernameNotFoundException("Could not find " + username));


    }

}
