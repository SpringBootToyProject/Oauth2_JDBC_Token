package com.springtoyproject.service;


import com.springtoyproject.common.exceptions.ErrorCodes;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.User;
import com.springtoyproject.domain.UserRole;
import com.springtoyproject.dto.ResetPasswordDto;
import com.springtoyproject.common.data.CommonResponseDto;
import com.springtoyproject.repository.UserRepository;
import com.springtoyproject.repository.UserRolesRepository;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRolesRepository userRolesRepository;

    private static Logger logger = Logger.getLogger("UserService");

    public User register(User user) throws ServiceException {
        if ((userRepository.findByUserName(user.getUserName()).isPresent()))
            throw new ServiceException("User already exists in database", ErrorCodes.DUPLICATE_ENTRY.getCode());
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        logger.info("request to save user: " + user);
        user = userRepository.save(user);
        UserRole userRole = new UserRole().setUserId(user.getUserId()).setRole("ROLE_USER");
        userRolesRepository.save(userRole);
        return user;
    }

    public CommonResponseDto reset(ResetPasswordDto resetPasswordDto) throws ServiceException {

        Optional<User> optionalUser = userRepository.findByUserName(resetPasswordDto.getUserName());
        if (!optionalUser.isPresent())
            throw new ServiceException("Cannot find the user " + resetPasswordDto.getUserName(), ErrorCodes.NO_ENTITY.getCode());
        User user = optionalUser.get();
        user.setPassword(new BCryptPasswordEncoder().encode(resetPasswordDto.getPassword()));
        userRepository.save(user);

        return new CommonResponseDto().setResult(CommonResponseDto.RESULT.SUCCESS);
    }

}


