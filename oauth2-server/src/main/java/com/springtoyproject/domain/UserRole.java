package com.springtoyproject.domain;

import javax.persistence.*;


@Entity
@Table(name = "auth_users_roles")
public class UserRole {

    @Id
    @SequenceGenerator(name = "auth_users_roles_id_seq", sequenceName = "auth_users_roles_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "auth_users_roles_id_seq")
    @Column(name = "id")
    private Long id;

    @Column(name = "userid")
    private Long userId;

    @Column(name = "role", columnDefinition = "nvarchar2(255)")
    private String role;

    public UserRole() {
    }

    public UserRole(Long userId, String role) {
        this.userId = userId;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public UserRole setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public UserRole setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public String getRole() {
        return role;
    }

    public UserRole setRole(String role) {
        this.role = role;
        return this;
    }

}
