package com.springtoyproject.dto;

import java.io.Serializable;


public class ResetPasswordDto implements Serializable {
    private String userName;
    private String password;

    public ResetPasswordDto() {
    }

    public String getUserName() {
        return userName;
    }

    public ResetPasswordDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public ResetPasswordDto setPassword(String password) {
        this.password = password;
        return this;
    }

}
