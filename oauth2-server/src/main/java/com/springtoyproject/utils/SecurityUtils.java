package com.springtoyproject.utils;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class SecurityUtils {
    public String authenticatedUser() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal().toString();

    }
}
