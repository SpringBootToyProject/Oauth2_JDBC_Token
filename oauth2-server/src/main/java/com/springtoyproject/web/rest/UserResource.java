package com.springtoyproject.web.rest;


import com.springtoyproject.common.data.SimpleRequestDto;
import com.springtoyproject.common.data.UserDto;
import com.springtoyproject.common.exceptions.ServiceException;
import com.springtoyproject.domain.User;
import com.springtoyproject.dto.ResetPasswordDto;
import com.springtoyproject.common.data.CommonResponseDto;
import com.springtoyproject.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class UserResource {

    @Autowired
    private UserService userService;

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> register(@Valid @RequestBody User user) throws ServiceException {
        return new ResponseEntity<>(userService.register(user), HttpStatus.OK);
    }


    @RequestMapping(value = "/password/reset", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<CommonResponseDto> reset(
            @RequestHeader String Authorization,
            @RequestBody ResetPasswordDto dto) throws ServiceException {
        return new ResponseEntity<>(userService.reset(dto), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST,value = "/signUp", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> signUp(@Valid @RequestBody UserDto userDto) throws ServiceException {
        return new ResponseEntity<>(userService.register(new User().setUserName(userDto.getUserName())
                .setPassword(userDto.getPassword())
                .setCreationDate(new Date())), HttpStatus.OK);
    }



    @RequestMapping(method = RequestMethod.POST, value = "/add",
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> add(@Valid @RequestBody SimpleRequestDto dto) throws ServiceException {
        return new ResponseEntity<>(userService.register(new User().setUserName(dto.getKey()).setPassword(UUID.randomUUID().toString())
                .setCreationDate(new Date())), HttpStatus.OK);
    }







}