package com.springtoyproject.aop;

import com.springtoyproject.common.data.CommonResponseDto;
import com.springtoyproject.common.exceptions.ErrorCodes;
import com.springtoyproject.common.exceptions.ServiceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<CommonResponseDto> exception(Exception exception, WebRequest request) {

        CommonResponseDto response = new CommonResponseDto();
        response.setResult(CommonResponseDto.RESULT.FAILURE);
//        response.setData(exception.getMessage());
        HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

        exception.printStackTrace();

        if (exception instanceof HttpMessageNotReadableException) {
            response.setData(exception.getMessage());
            response.setErrorCode(ErrorCodes.BAD_INPUT.getCode());

        } else if (exception instanceof ServiceException) {
            int ErrorCode = ((ServiceException) exception).getErrorCode();

            response.setData(exception.getMessage());
            response.setErrorCode(ErrorCode);


            if (ErrorCode == ErrorCodes.ACCESS_DENIED.getCode()
                    || ErrorCode == ErrorCodes.INACTIVE_USER.getCode()
                    || ErrorCode == ErrorCodes.NO_USER.getCode()) {

                status = HttpStatus.FORBIDDEN;

            } else if (ErrorCode == ErrorCodes.NO_ENTITY.getCode()) {
                status = HttpStatus.NOT_FOUND;
            }

        } else if (exception instanceof AccessDeniedException) {
            response.setData("Access denied");
            response.setErrorCode(ErrorCodes.ACCESS_DENIED.getCode());
            status = HttpStatus.FORBIDDEN;

        } else if (exception instanceof org.springframework.web.bind.MethodArgumentNotValidException) {
            response.setData("input data is not valid. " );
            response.setErrorCode(ErrorCodes.VALIDATION_ERROR.getCode());
            status = HttpStatus.BAD_REQUEST;

        }else if (exception instanceof org.springframework.dao.DataIntegrityViolationException) {
            response.setData("input data integrity is not correct, maybe some essential data does not entered.");
            response.setErrorCode(ErrorCodes.BAD_INPUT.getCode());
            status = HttpStatus.BAD_REQUEST;

        } else if (exception instanceof RuntimeException) {
            response.setData("Internal error");
            response.setErrorCode(ErrorCodes.INTERNAL_ERROR.getCode());
            status = HttpStatus.INTERNAL_SERVER_ERROR;

        }

        return new ResponseEntity<>(response, status);
    }

}