package com.springtoyproject.repository;


import com.springtoyproject.domain.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRolesRepository extends JpaRepository<UserRole, Long> {

    @Query("select a.role from UserRole a, User b where b.userName=?1 and a.userId=b.userId")
    List<String> findRoleByUserName(String username);

    void deleteByUserId(long userId);

}