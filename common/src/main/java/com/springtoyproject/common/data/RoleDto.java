package com.springtoyproject.common.data;


import java.io.Serializable;

public class RoleDto implements Serializable {
    private long id;
    private String name;
    private String description;

    public RoleDto() {
    }

    public long getId() {
        return id;
    }

    public RoleDto setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public RoleDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public RoleDto setDescription(String description) {
        this.description = description;
        return this;
    }
}
