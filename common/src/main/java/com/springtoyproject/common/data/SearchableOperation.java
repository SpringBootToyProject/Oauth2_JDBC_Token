package com.springtoyproject.common.data;

public enum SearchableOperation {
    EQUALS,
    LIKE,
    BIGGER,
    LESSER,
    IN
}
