package com.springtoyproject.common.data;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.util.Iterator;
import java.util.List;

public class ResultPageDto<T> implements Page {

    private Page page;
    private List<T> content;
    private long totalElements;
    private int totalPages;
    private Sort sort;
    private int number;
    private int size;
    private int numberOfElements;
    private boolean isLast;
    private boolean isFirst;

    public ResultPageDto() {
    }

    public ResultPageDto(Page page, List<T> content) {
        this.page = page;
        this.content = content;

        if (page != null) {
            totalElements = page.getTotalElements();
            totalPages = page.getTotalPages();

        }
    }

    public int getTotalPages() {
        if (page == null) return this.totalPages;
        return page.getTotalPages();
    }

    public long getTotalElements() {
        if (page == null) return this.totalElements;
        return page.getTotalElements();
    }

    public Page map(Converter converter) {
        return null;
    }

    public int getNumber() {
        if (page == null) return this.number;
        return page.getNumber();
    }

    public int getSize() {
        if (page == null) return this.size;
        return page.getSize();
    }

    public int getNumberOfElements() {
        if (page == null) return this.numberOfElements;
        return page.getNumberOfElements();
    }

    public List getContent() {
        return content;
    }

    public boolean hasContent() {
        return (content != null && content.size() > 0);
    }

    public Sort getSort() {
        if (page == null) return this.sort;
        return page.getSort();
    }

    public boolean isFirst() {
        if (page == null) return this.isFirst;
        return page.isFirst();
    }

    public boolean isLast() {
        if (page == null) return this.isLast;
        return page.isLast();
    }

    public boolean hasNext() {
        return page.hasNext();
    }

    public boolean hasPrevious() {
        return page.hasPrevious();
    }

    public Pageable nextPageable() {
        return null;
    }

    public Pageable previousPageable() {
        return null;
    }

    public Iterator<T> iterator() {
        return content.iterator();
    }

    @JsonDeserialize(using = ResultSortDtoDeserializer.class)
    public void setSort(Sort sort) {
        this.sort = sort;
    }

    public ResultPageDto setTotalElements(long totalElements) {
        this.totalElements = totalElements;
        return this;
    }

    public ResultPageDto setTotalPages(int totalPages) {
        this.totalPages = totalPages;
        return this;
    }

    public ResultPageDto setNumber(int number) {
        this.number = number;
        return this;
    }

    public ResultPageDto setSize(int size) {
        this.size = size;
        return this;
    }

    public ResultPageDto setNumberOfElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
        return this;
    }

    public ResultPageDto setIsLast(boolean isLast) {
        this.isLast = isLast;
        return this;
    }

    public ResultPageDto setIsFirst(boolean isFirst) {
        this.isFirst = isFirst;
        return this;
    }

}
