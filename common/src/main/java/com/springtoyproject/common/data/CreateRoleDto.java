package com.springtoyproject.common.data;


import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

public class CreateRoleDto {

    private long id;

    @NotNull
    private String name;
    private String description;

    @NotNull
    private Set<String> permissions= new HashSet<String>();

    public long getId() {
        return id;
    }

    public CreateRoleDto setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public CreateRoleDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CreateRoleDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public Set<String> getPermissions() {
        return permissions;
    }

    public CreateRoleDto setPermissions(Set<String> permissions) {
        this.permissions = permissions;
        return this;
    }
}
