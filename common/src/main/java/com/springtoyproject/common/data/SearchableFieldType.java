package com.springtoyproject.common.data;

public enum SearchableFieldType {
    COLLECTION,
    JOIN
}
