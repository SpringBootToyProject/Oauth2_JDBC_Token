package com.springtoyproject.common.data;

import java.io.Serializable;


public class UserDto implements Serializable{

    private String userName;
    private String password;

    public UserDto() {
    }

    public String getUserName() {
        return userName;
    }

    public UserDto setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDto setPassword(String password) {
        this.password = password;
        return this;
    }

}
