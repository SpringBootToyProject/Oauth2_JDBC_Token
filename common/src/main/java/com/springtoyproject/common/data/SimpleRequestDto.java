package com.springtoyproject.common.data;

import java.io.Serializable;

public class SimpleRequestDto implements Serializable {
    private String key;

    public SimpleRequestDto() {
    }

    public SimpleRequestDto(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public SimpleRequestDto setKey(String key) {
        this.key = key;
        return this;
    }
}
