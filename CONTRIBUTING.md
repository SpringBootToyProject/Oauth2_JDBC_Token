commit convention:

    There are 2 main branches: master, developer
    master: developer can not commit in this branch ( users who have master grant access can commit and merge this)
            It is recommended to only merge this branch with the developer branch without any code development 
    developer: users who have master or developer grant can commit and merge this branch

How to run the project:

    1) discovery-server
    2) gateway-service
    3) config-server
    4) the project
    
commit messages must follow this instruction:

    ((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing))(:?) +(?:(?:issues? +)?%{issue_ref}(?:(?:, *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)
    
    This translates to the following keywords:
    
        Close, Closes, Closed, Closing, close, closes, closed, closing
        Fix, Fixes, Fixed, Fixing, fix, fixes, fixed, fixing
        Resolve, Resolves, Resolved, Resolving, resolve, resolves, resolved, resolving
    
    For example the following commit message:
    
    Awesome commit message
    
    Fix #20, Fixes #21 and Closes group/otherproject#22.
    This commit is also related to #17 and fixes #18, #19
    and https://gitlab.example.com/group/otherproject/issues/23.
    
    will close #18, #19, #20, and #21 in the project this commit is pushed to, as well as #22 and #23 in group/otherproject. #17 won't be closed as it does not match the pattern. It works with multi-line commit messages as well as one-liners when used with git commit -m